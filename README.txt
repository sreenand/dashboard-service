This project is built on Dropwizard 1.3.5.  This project uses Google Guice libraries as well.

Please read about dropwizard from https://www.dropwizard.io

Java 1.8 & Maven latest version are required for this project to run without any hurdles.

How to run this project on Intellij ?

https://blog.indrek.io/articles/running-a-dropwizard-application-in-intellij-eclipse-and-netbeans/


How to package this project ? 

go to project folder run the command `mvn package`. A fat jar (dashboard-service-1.0-SNAPSHOT.jar) will be created under the folder name target.

What will be packaging format ?
Jar 

How to run a packaged jar?

java -jar target/dashboard-service-1.0-SNAPSHOT.jar server payments-service.yml.


Where to put config parameters like db name, username & password etc.,?

dashboard-service.yml

How to set up this project in Intellij?

Checkout the repo. Open intellj go to file->open->project folder. Click on the pom.xml intellij will auto resolve dependecies & set up things for you.

