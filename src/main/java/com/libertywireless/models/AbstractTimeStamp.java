package com.libertywireless.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass @Setter @Getter
public class AbstractTimeStamp implements Serializable {

    @Column(name = "created_at")
    @JsonProperty("created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", timezone="IST") //ISO8601 format
    protected Date createdAt;

    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", timezone="IST") //ISO8601 format
    @Version
    protected Date updatedAt;  //Optimistic locking


    public AbstractTimeStamp()  {
        createdAt = new Date();
    }
}
