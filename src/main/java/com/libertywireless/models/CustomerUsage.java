package com.libertywireless.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor @Table(name = "customer_usage")
public class CustomerUsage extends AbstractTimeStamp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String msisdn;

    @Column(name = "current_data")
    private int currentData;
    @Column(name = "total_data")
    private int totalData;

    @Column(name = "current_talk_time")
    private int currentTalkTime;
    @Column(name = "total_talk_time")
    private int totalTalkTime;

    @Column(name = "current_sms")
    private int currentSms;
    @Column(name = "total_sms")
    private int totalSms;

}
