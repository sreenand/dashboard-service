package com.libertywireless;

import com.google.inject.Stage;
import com.hubspot.dropwizard.guice.GuiceBundle;
import com.libertywireless.configuration.DashboardServiceConfiguration;
import com.libertywireless.models.CustomerUsage;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.hibernate.cfg.Configuration;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class DashboardServiceApplication extends Application<DashboardServiceConfiguration> {

    private final HibernateBundle<DashboardServiceConfiguration> hibernate = new HibernateBundle<DashboardServiceConfiguration>(
            CustomerUsage.class) {

        public DataSourceFactory getDataSourceFactory(DashboardServiceConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }

        @Override
        public void configure(Configuration configuration) {
            configuration.addPackage("com.awign.models");
        }
    };

    @Override
    public void initialize(Bootstrap<DashboardServiceConfiguration> bootstrap) {

        GuiceBundle<DashboardServiceConfiguration> guiceBundle = GuiceBundle.<DashboardServiceConfiguration>newBuilder()
                .addModule(new DashboardServiceModule()).addModule(new HIbernateModule(hibernate))
                .enableAutoConfig(getClass().getPackage().getName())
                .setConfigClass(DashboardServiceConfiguration.class)
                .build(Stage.DEVELOPMENT);

        bootstrap.addBundle(new AssetsBundle("/public", "/assets"));
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(guiceBundle);

    }


    public void run(DashboardServiceConfiguration configuration, Environment environment) throws Exception {

        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        environment.getApplicationContext().setSessionHandler(new SessionHandler());
    }


    public static void main(String[] args) throws Exception {
        new DashboardServiceApplication().run(args);
    }
}
