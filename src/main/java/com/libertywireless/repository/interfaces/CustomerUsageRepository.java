package com.libertywireless.repository.interfaces;

import com.libertywireless.models.CustomerUsage;

public interface CustomerUsageRepository {


    public boolean checkIfCustomerExists(String phoneNo);

    public CustomerUsage getAllCustomerUsage(String phoneNo);

    public CustomerUsage uppdateData(String phoneNo);

}
