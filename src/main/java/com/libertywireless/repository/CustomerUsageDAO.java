package com.libertywireless.repository;

import com.libertywireless.models.CustomerUsage;
import com.libertywireless.repository.interfaces.CustomerUsageRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class CustomerUsageDAO extends AbstractDAO<CustomerUsage> implements CustomerUsageRepository {

    public CustomerUsageDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public CustomerUsage create(CustomerUsage entity) {
        currentSession().persist(entity);
        return entity;
    }

    @Override
    public boolean checkIfCustomerExists(String phoneNo) {
        List<CustomerUsage> entities = list(currentSession().createCriteria(CustomerUsage.class).
                add(Restrictions.eq("msisdn", phoneNo)));
        return (entities.size() > 0 ? true:false);
    }

    @Override
    public CustomerUsage getAllCustomerUsage(String phoneNo) {
        List<CustomerUsage> entities = list(currentSession().createCriteria(CustomerUsage.class).
                add(Restrictions.eq("msisdn", phoneNo)));
        return (entities.size() > 0 ? entities.get(0):null);
    }

    @Override
    public CustomerUsage uppdateData(String phoneNo) {
        List<CustomerUsage> entities = list(currentSession().createCriteria(CustomerUsage.class).
                add(Restrictions.eq("msisdn", phoneNo)));
        CustomerUsage entity =  (entities.size() > 0 ? entities.get(0):null);

        if(entity == null) {
            return null;
        } else {
            entity.setTotalData(entity.getTotalData() + 1);
        }
        currentSession().persist(entity);
        return entity;
    }
}
