package com.libertywireless;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.libertywireless.configuration.DashboardServiceConfiguration;
import com.libertywireless.repository.CustomerUsageDAO;
import com.libertywireless.repository.interfaces.CustomerUsageRepository;
import io.dropwizard.hibernate.HibernateBundle;

public class HIbernateModule extends AbstractModule {

    private HibernateBundle<DashboardServiceConfiguration> hibernateBundle;


    public HIbernateModule(HibernateBundle<DashboardServiceConfiguration> hibernate) {
        this.hibernateBundle = hibernate;
    }

    @Override
    protected void configure() {

    }

    @Provides
    public CustomerUsageRepository provideDemandRepo(){
        return new CustomerUsageDAO(hibernateBundle.getSessionFactory());
    }

}
