package com.libertywireless;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.libertywireless.configuration.DashboardServiceConfiguration;
import com.libertywireless.services.CustomerUsageService;
import com.libertywireless.services.interfaces.CustomerUsageServiceInterface;
import io.dropwizard.jetty.ConnectorFactory;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.server.DefaultServerFactory;

public class DashboardServiceModule extends AbstractModule {

    protected void configure() {
        bind(CustomerUsageServiceInterface.class).to(CustomerUsageService.class);

    }

    @Provides
    public HttpConnectorFactory provideServerConfig(DashboardServiceConfiguration configuration)  {
        HttpConnectorFactory httpConnector = null;
        DefaultServerFactory serverFactory = (DefaultServerFactory) configuration.getServerFactory();
        for (ConnectorFactory connector : serverFactory.getApplicationConnectors()) {
            if (connector.getClass().isAssignableFrom(HttpConnectorFactory.class)) {
                httpConnector = (HttpConnectorFactory) connector;
            }
        }
        return httpConnector;
    }

    @Provides @Singleton
    public ObjectMapper provideMapper() {
        return new ObjectMapper();
    }

}
