package com.libertywireless.resources;


import com.libertywireless.models.CustomerUsage;
import com.libertywireless.services.interfaces.CustomerUsageServiceInterface;
import io.dropwizard.hibernate.UnitOfWork;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/customer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {


    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CustomerResource.class);
    private CustomerUsageServiceInterface entityService;

    @Inject
    public CustomerResource(CustomerUsageServiceInterface entityService) {
        this.entityService = entityService;
    }


    @POST
    @Path("/iscustomer") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response IsCustomer(@Valid Map<String, Object> parameters){

        try{
            String msisdn = (String)parameters.get("msisdn");
            Map<String, Object> response = new HashMap<String, Object>();
            response.put("response", new Boolean(entityService.checkEntityExists(msisdn)));
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.error("Exception in processing request", e.getMessage());
            logger.error("Exception in processing request", e);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

    }

    @POST
    @Path("/currentusage") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getCurrentUsage(@Valid Map<String, Object> parameters){

        try{
            String msisdn = (String)parameters.get("msisdn");
            Map<String, Object> response = new HashMap<String, Object>();
            response.put("current_usage", entityService.getEntityUsage(msisdn));
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.error("Exception in processing request", e.getMessage());
            logger.error("Exception in processing request", e);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

    }

    @POST
    @Path("/boost") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response applyBoost(@Valid Map<String, Object> parameters){

        try{
            String msisdn = (String)parameters.get("msisdn");
            Map<String, Object> response = new HashMap<String, Object>();
            response.put("current_usage", entityService.postBoost(msisdn));
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.error("Exception in processing request", e.getMessage());
            logger.error("Exception in processing request", e);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

    }
}
