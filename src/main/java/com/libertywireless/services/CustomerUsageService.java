package com.libertywireless.services;

import com.libertywireless.models.CustomerUsage;
import com.libertywireless.repository.interfaces.CustomerUsageRepository;
import com.libertywireless.resources.CustomerResource;
import com.libertywireless.services.interfaces.CustomerUsageServiceInterface;

import javax.inject.Inject;

public class CustomerUsageService implements CustomerUsageServiceInterface {

    private CustomerUsageRepository customerUsageRepository;

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CustomerUsageService.class);

    @Inject
    public CustomerUsageService(CustomerUsageRepository customerUsageRepository) {
        this.customerUsageRepository = customerUsageRepository;
    }

    @Override
    public boolean checkEntityExists(String phoneNo) {

        logger.error("Inside the Customer Usage Service");
        return customerUsageRepository.checkIfCustomerExists(phoneNo);

    }

    @Override
    public CustomerUsage getEntityUsage(String phoneNo) {
        return customerUsageRepository.getAllCustomerUsage(phoneNo);
    }

    @Override
    public CustomerUsage postBoost(String phoneNo) {
        return customerUsageRepository.uppdateData(phoneNo);
    }
}
