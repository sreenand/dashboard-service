package com.libertywireless.services.interfaces;

import com.libertywireless.models.CustomerUsage;

public interface CustomerUsageServiceInterface {

    public boolean checkEntityExists (String phoneNo);

    public CustomerUsage getEntityUsage (String phoneNo);

    public CustomerUsage postBoost (String phoneNo);
}
